#!/bin/bash
p=$(ping -c 1 -W 2 $1 2> /dev/null | grep "time=.*ms" -o | grep -o "[0-9\.]*")
echo $p ms
