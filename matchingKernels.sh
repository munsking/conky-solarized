#!/bin/bash
if [ $(uname -r) == $(~/.config/conky/installedKernel.sh) ]
then
	echo '${color 859900}'
else
	echo '${color dc322f}'
fi
