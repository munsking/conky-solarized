#!/bin/bash
name1=$(nvidia-smi -q -a | grep -i "name" | grep -o ":.*$" | cut -b 3- | sed -n 1p)
name2=$(nvidia-smi -q -a | grep -i "name" | grep -o ":.*$" | cut -b 3- | sed -n 2p)
temp1=$(nvidia-smi -q -a | grep -i "GPU Current" | grep "[0-9]*" -o | sed -n 1p)
temp2=$(nvidia-smi -q -a | grep -i "GPU Current" | grep "[0-9]*" -o | sed -n 2p)
fan1=$(nvidia-smi -q -a | grep -i "fan" | grep "[0-9]*" -o | sed -n 1p)
fan2=$(nvidia-smi -q -a | grep -i "fan" | grep "[0-9]*" -o | sed -n 2p)
echo "$name1      $temp1°C             $fan1%"
echo "$name2      $temp2°C             $fan2%"
